package bomdestino.safim.middleware.config.security;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    private Long id;
    private String nome;
    private List<String> permissions;

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public User setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public User setPermissions(List<String> permissions) {
        this.permissions = permissions;
        return this;
    }
}
