
package bomdestino.safim.middleware.config.security;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.io.IOException;

import static bomdestino.safim.middleware.util.enums.PermissaoUsuarioEnum.ADMINISTRATIVE;
import static bomdestino.safim.middleware.util.enums.PermissaoUsuarioEnum.SISTEMIC;
import static bomdestino.safim.middleware.util.enums.PermissaoUsuarioEnum.SUPORT;
import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;

@Configuration
@EnableResourceServer
@EnableConfigurationProperties(SecurityProperties.class)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String ROOT_PATTERN_PUBLIC = "/api/public/**";
    private static final String ROOT_PATTERN_PRIVATE = "/api/private/**";

    private final SecurityProperties securityProperties;

    private TokenStore tokenStore;

    public ResourceServerConfiguration(final SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

    @Override
    public void configure(final ResourceServerSecurityConfigurer resources) {
        resources.tokenStore(tokenStore());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("read"))
                .antMatchers(HttpMethod.POST, ROOT_PATTERN_PUBLIC).access(obterAcessoPrivado("write"))
                .antMatchers(HttpMethod.PUT, ROOT_PATTERN_PUBLIC).access(obterAcessoPrivado("write"))
                .antMatchers(HttpMethod.PATCH, ROOT_PATTERN_PUBLIC).access(obterAcessoPrivado("read"))
                .antMatchers(HttpMethod.DELETE, ROOT_PATTERN_PUBLIC).access(obterAcessoPrivado("write"));

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("read"))
                .antMatchers(HttpMethod.POST, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("write"))
                .antMatchers(HttpMethod.PUT, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("write"))
                .antMatchers(HttpMethod.PATCH, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("read"))
                .antMatchers(HttpMethod.DELETE, ROOT_PATTERN_PRIVATE).access(obterAcessoPrivado("write"));
    }

    @Bean
    public TokenStore tokenStore() {
        if (tokenStore == null) {
            tokenStore = new JwtTokenStore(jwtAccessTokenConverter());
        }
        return tokenStore;
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setVerifierKey(getPublicKeyAsString());
        DefaultAccessTokenConverter defaultAccessTokenConverter = new DefaultAccessTokenConverter();
        defaultAccessTokenConverter.setUserTokenConverter(new UserAuthenticationConverter());
        converter.setAccessTokenConverter(defaultAccessTokenConverter);
        return converter;
    }

    private String getPublicKeyAsString() {
        try {
            return IOUtils.toString(securityProperties.getJwt().getPublicKey().getInputStream(), UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String obterAcessoPrivado(String escopo) {
        return format("#oauth2.hasScope('%s') and hasAuthority('%s') or hasAuthority('%s') or hasAuthority('%s')",
                escopo, ADMINISTRATIVE, SISTEMIC, SUPORT);
    }
}
