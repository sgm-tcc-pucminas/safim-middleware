package bomdestino.safim.middleware.config.security;


import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserAuthenticationConverter extends DefaultUserAuthenticationConverter {

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        User user = new User()
                .setId(new Long( (Integer) map.get("id")))
                .setNome((String) map.get("nome"));

        List<SimpleGrantedAuthority> authorities = null;
        if (map.get("authorities") != null) {

            List<String> authList = (List<String>) map.get("authorities");

            authorities = (authList)
                    .stream()
                    .map(a -> new SimpleGrantedAuthority(a))
                    .collect(Collectors.toList());

            user.setPermissions(authList);
        }

        return new UsernamePasswordAuthenticationToken(user, null, authorities);
    }
}