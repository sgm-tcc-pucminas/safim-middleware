package bomdestino.safim.middleware.controller;

import bomdestino.safim.middleware.model.dto.ExecucaoServicoDTO;
import bomdestino.safim.middleware.model.dto.ServicoResponsavelDTO;
import bomdestino.safim.middleware.model.dto.ServicoResponsavelReponseDTO;
import bomdestino.safim.middleware.model.dto.ServicoStatusDTO;
import bomdestino.safim.middleware.service.ServicoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/private")
public class ServicoPrivateService {

    @Autowired
    private ServicoService servicoService;

    @ApiOperation(
            value = "Objetivo Empresas Execução Serviço",
            notes = "Obtem as empresas/pessoal cadastrado para a execução do serviço informado",
            response = ServicoResponsavelReponseDTO.class
    )
    @GetMapping("/v1/servico/responsavel/{idServico}")
    public ResponseEntity<ServicoResponsavelReponseDTO> obterResponsavel(@PathVariable("idServico") Long idServico) {
        List<ServicoResponsavelDTO> empresas = servicoService.obterEmpresaServico(idServico);

        if (empresas.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new ServicoResponsavelReponseDTO(empresas));
        }
    }

    @ApiOperation(
            value = "Execução Serviço",
            notes = "Registra solicitacao de execução do serviço informado"
    )
    @PostMapping("/v1/servico")
    public ResponseEntity<Void> executarServico(@RequestBody ExecucaoServicoDTO servicoExecucao) {
        servicoService.executarServico(servicoExecucao);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(
            value = "Obter Status Execução Serviço",
            notes = "Obter o status da solicitacao de execução de serviço informada",
            response = ServicoStatusDTO.class
    )
    @GetMapping("/v1/servico/{idSolicitacao}")
    public ResponseEntity<ServicoStatusDTO> executarServico(@PathVariable("idSolicitacao") Long idSolicitacao) {
        ServicoStatusDTO servicoStatus = servicoService.obterStatusSolicitacao(idSolicitacao);
        return ResponseEntity.ok().body(servicoStatus);
    }
}