package bomdestino.safim.middleware.service;

import bomdestino.safim.middleware.model.dto.ExecucaoServicoDTO;
import bomdestino.safim.middleware.model.dto.ServicoResponsavelDTO;
import bomdestino.safim.middleware.model.dto.ServicoStatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static java.lang.String.format;
import static java.util.Arrays.asList;

@Service
public class ServicoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public List<ServicoResponsavelDTO> obterEmpresaServico(Long idServico) {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n[*** SAFIM MIDDLEWARE ***] ---------------------------------------------------------");
        mensagem.append(format("\n[*** SAFIM MIDDLEWARE ***] Obtendo executores do serviço id %d", idServico));
        mensagem.append("\n[*** SAFIM MIDDLEWARE ***] ---------------------------------------------------------\n\n");
        logger.info(mensagem.toString());

        return asList(new ServicoResponsavelDTO()
                        .setId(1L)
                        .setNome("Huguinho")
                        .setDocumentoIdentificacao("987.654.321-00")
                        .setTelefone("(19) 987654321")
                        .setEmail("huguinho@bomdestino.sp.gov.br"),
                new ServicoResponsavelDTO()
                        .setId(2L)
                        .setNome("Zezinho")
                        .setDocumentoIdentificacao("987.654.321-01")
                        .setTelefone("(19) 987654321")
                        .setEmail("zezinho@bomdestino.sp.gov.br"),
                new ServicoResponsavelDTO()
                        .setId(3L)
                        .setNome("Luizinho")
                        .setDocumentoIdentificacao("987.654.321-02")
                        .setTelefone("(19) 987654321")
                        .setEmail("luizinho@bomdestino.sp.gov.br"));
    }

    public void executarServico(ExecucaoServicoDTO servico) {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SAFIM MIDDLEWARE ***] ---------------------------------------------------------");
        mensagem.append("\n\n responsavel: " + servico.getResponsavel());
        mensagem.append("\n\n contato: " + servico.getContato());
        mensagem.append("\n\n serviço: " + servico.getServico());
        mensagem.append("\n\n endereco: " + servico.getEndereco().getEndereco());
        mensagem.append("\n\n cep: " + servico.getEndereco().getCep());
        mensagem.append("\n\n observacao: " + servico.getEndereco().getObservacao());
        mensagem.append("\n\n url: " + servico.getEndereco().getUrlMapa());
        mensagem.append("\n\n[*** SAFIM MIDDLEWARE ***] ---------------------------------------------------------\n\n");
        logger.info(mensagem.toString());
    }

    public ServicoStatusDTO obterStatusSolicitacao(Long idSolicitacao) {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SAFIM MIDDLEWARE ***] ---------------------------------------------------------");
        mensagem.append("\n\n [*** SAFIM MIDDLEWARE ***] OBTENDO STATUS DA SOLICITACAO ID: " + idSolicitacao);
        mensagem.append("\n\n [*** SAFIM MIDDLEWARE ***] ---------------------------------------------------------\n\n");
        logger.info(mensagem.toString());

        return new ServicoStatusDTO()
                .setIdSolicitacaoServico(idSolicitacao)
                .setDataHoraExecucao(LocalDateTime.now());
    }
}