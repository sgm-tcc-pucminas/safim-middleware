package bomdestino.safim.middleware.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoDTO implements Serializable {

    private Long idServico;
    private String nomeServico;
    private BigDecimal valor;
    private Integer prazoAtendimento;
    private String unidadeMedidaPrazo;

    public Long getIdServico() {
        return idServico;
    }

    public ServicoDTO setIdServico(Long idServico) {
        this.idServico = idServico;
        return this;
    }

    public String getNomeServico() {
        return nomeServico;
    }

    public ServicoDTO setNomeServico(String nomeServico) {
        this.nomeServico = nomeServico;
        return this;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public ServicoDTO setValor(BigDecimal valor) {
        this.valor = valor;
        return this;
    }

    public Integer getPrazoAtendimento() {
        return prazoAtendimento;
    }

    public ServicoDTO setPrazoAtendimento(Integer prazoAtendimento) {
        this.prazoAtendimento = prazoAtendimento;
        return this;
    }

    public String getUnidadeMedidaPrazo() {
        return unidadeMedidaPrazo;
    }

    public ServicoDTO setUnidadeMedidaPrazo(String unidadeMedidaPrazo) {
        this.unidadeMedidaPrazo = unidadeMedidaPrazo;
        return this;
    }
}