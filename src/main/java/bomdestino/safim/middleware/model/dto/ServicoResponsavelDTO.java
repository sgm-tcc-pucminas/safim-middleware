package bomdestino.safim.middleware.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoResponsavelDTO implements Serializable {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("documentoIdentificacao")
    private String documentoIdentificacao;
    @JsonProperty("telefone")
    private String telefone;
    @JsonProperty("email")
    private String email;

    public Long getId() {
        return id;
    }

    public ServicoResponsavelDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public ServicoResponsavelDTO setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getDocumentoIdentificacao() {
        return documentoIdentificacao;
    }

    public ServicoResponsavelDTO setDocumentoIdentificacao(String documentoIdentificacao) {
        this.documentoIdentificacao = documentoIdentificacao;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public ServicoResponsavelDTO setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ServicoResponsavelDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServicoResponsavelDTO that = (ServicoResponsavelDTO) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}