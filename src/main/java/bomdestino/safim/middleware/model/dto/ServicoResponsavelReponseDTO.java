package bomdestino.safim.middleware.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoResponsavelReponseDTO implements Serializable {

    @JsonProperty("responsaveis")
    private List<ServicoResponsavelDTO> responsaveis;

    public ServicoResponsavelReponseDTO() {
    }

    public ServicoResponsavelReponseDTO(List<ServicoResponsavelDTO> empresas) {
        this.responsaveis = empresas;
    }

    public List<ServicoResponsavelDTO> getResponsaveis() {
        return responsaveis;
    }

    public ServicoResponsavelReponseDTO setResponsaveis(List<ServicoResponsavelDTO> responsaveis) {
        this.responsaveis = responsaveis;
        return this;
    }
}