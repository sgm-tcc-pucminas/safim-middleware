package bomdestino.safim.middleware.model.dto;

import java.io.Serializable;

public class ExecucaoServicoDTO implements Serializable {

    private ServicoDTO servico;
    private EnderecoDTO endereco;

    private Long idUsuarioSolicitacao;
    private String responsavel;
    private String contato;

    public ServicoDTO getServico() {
        return servico;
    }

    public ExecucaoServicoDTO setServico(ServicoDTO servico) {
        this.servico = servico;
        return this;
    }

    public EnderecoDTO getEndereco() {
        return endereco;
    }

    public ExecucaoServicoDTO setEndereco(EnderecoDTO endereco) {
        this.endereco = endereco;
        return this;
    }

    public Long getIdUsuarioSolicitacao() {
        return idUsuarioSolicitacao;
    }

    public ExecucaoServicoDTO setIdUsuarioSolicitacao(Long idUsuarioSolicitacao) {
        this.idUsuarioSolicitacao = idUsuarioSolicitacao;
        return this;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public ExecucaoServicoDTO setResponsavel(String responsavel) {
        this.responsavel = responsavel;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public ExecucaoServicoDTO setContato(String contato) {
        this.contato = contato;
        return this;
    }
}
